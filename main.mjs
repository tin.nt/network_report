import { readdirSync, readFileSync, writeFileSync } from 'fs'
import _ from 'lodash'
import doc from 'docx'

import config from './config'

const Document = doc.Document;
const Table = doc.Table;
const Packer = doc.Packer;
const Paragraph = doc.Paragraph;
const WidthType = doc.WidthType;
// const DocStyles = new doc.Styles();

// DocStyles
//   .createParagraphStyle('main_content')
//   .size(13);
//
// DocStyles.createParagraphStyle('table_title')
//   .bold()
//   .size(15);

class Report {
  constructor() {
    this.dirInput = config.PATH_DIR_TXT;
    this.dirOutput = config.PATH_DIR_DOCX;

    this.DOCX = new Document({
      creator: 'Chau Quang Le',
      description: 'Report internet log',
      title: 'Report log system',
    });

    this.PACKER = new Packer();

    this.initDoc()
  }

  initDoc() {
    const paragraph = new Paragraph('MAINTENANCE REPORT INFORMATION TECHNOLOGY SYSTEM');
    paragraph.heading1();
    paragraph.center();

    this.DOCX.addParagraph(paragraph)
  }

  retrievingFilesDir() {
    const files = readdirSync(this.dirInput);

    _.forEach(files, (file) => {
      const pathFile = `${this.dirInput}/${file}`;
      this.readFile(pathFile, file);

      const paragraph = new Paragraph('------------------------------------------------------------------------------');
      paragraph.heading1();
      this.DOCX.addParagraph(paragraph);
    });

    this.PACKER.toBuffer(this.DOCX).then((buffer) => {
      writeFileSync(`${this.dirOutput}/report.docx`, buffer);
    });
  }

  readFile(file, fileName) {
    const lines = readFileSync(file, 'utf-8').split(`\n`);
    const table = new Table(2, 3);

    // Table Format.
    table.setWidth(800, WidthType.NIL);
    table.getRow(0).mergeCells(1, 2);

    const CELL = {
      NO: table.getCell(0, 0),
      TITLE: table.getCell(0, 1),
      INFO: table.getCell(1, 1),
      LOG: table.getCell(1, 2)
    };

    // Add content table.
    CELL.NO.addContent(
      new Paragraph('No').heading3().center()
    );

    CELL.TITLE.addContent(
      new Paragraph(`******** ${fileName} ********`).heading3().center()
    );

    this.DOCX.addTable(table);

    _.forEach(lines, (data, key) => {
      console.log('Line: ' + key);
      console.log(data);
    });

    CELL.INFO.addContent(
      new Paragraph('Name: BYT-SW2-LV126\n' +
        'Location: BYT-SW2-LV126\n' +
        'IP: 172.16.2.128\n' +
        'Up Time: 3 days 8 hours 44 minutes\n' +
        'Curr CPU: 32%\n' +
        'Curr Memory: 76%\n' +
        'Have 1 Chasis').heading3().left()
    );

    CELL.LOG.addContent(
      new Paragraph('----+----+---------+------+-----------+----------\n' +
        'Chasis 1 : OS6450-P10- 8 POE 10/100/1000 + 2 Combo + 2 5G STK/UPLINK\n' +
        'Serial Number: R4588172\n' +
        'Temperature: = 58\n' +
        'PSU: 1 1 120 AC UP Internal | \n' +
        'FAN: \n' +
        '----+----+---------+------+-----------+----------\n' +
        '|Port 1/8<--------> Port 1/10|172.16.2.123|BYT-LV123\n' +
        '|Port 1/9<--------> Port 1/10|172.16.2.126|GOLFCLUB-OFFICE2\n' +
        '|Port 1/12<--------> Port 2/12|172.16.2.1|BYT-SW3-01-IDC-ELEC').heading3().left()
    );
  }

  process() {
    this.retrievingFilesDir()
  }
}

const report = new Report();
report.process();